/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "diagramview.h"
#include "arrow.h"
#include "qdebug.h"

#include <QTextCursor>
#include <QGraphicsSceneMouseEvent>
#include <QToolButton>
#include <QGridLayout>

//! [0]
DiagramView::DiagramView(QMenu *itemMenu, QObject *parent)
{
	myItemMenu = itemMenu;
	myMode = MoveItem;
	myItemType = DiagramItem::Capture;
	line = 0;
	textItem = 0;
	myItemColor = Qt::white;
	myTextColor = Qt::black;
	myLineColor = Qt::black;
	scene = new QGraphicsScene(this);
	this->setScene(scene);
	setAcceptDrops(true);
	this->show();


	connect(scene, SIGNAL(itemSelected(QGraphicsItem*)),
			this, SLOT(itemSelected(QGraphicsItem*)));

//	QPixmap p;
//	item = new DiagramItem(DiagramItem::Capture, itemMenu);
//	item->setBrush(QBrush(Qt::black));
//	scene->addItem(item);
//	this->hide();

}
//! [0]

//! [1]

void DiagramView::setMode(Mode mode)
{
	myMode = mode;
}

void DiagramView::setItemType(DiagramItem::DiagramType type)
{
	myItemType = type;
}

void DiagramView::itemSelected(QGraphicsItem *item)
{
	qDebug("item selected bae");
}

//! [5]
//void DiagramView::editorLostFocus(DiagramTextItem *item)
//{
//	QTextCursor cursor = item->textCursor();
//	cursor.clearSelection();
//	item->setTextCursor(cursor);

//	if (item->toPlainText().isEmpty()) {
//		removeItem(item);
//		item->deleteLater();
//	}
//}
//! [5]

//! [6]
void DiagramView::mousePressEvent(QMouseEvent *mouseEvent)
{
	if (mouseEvent->button() != Qt::LeftButton)
		return;
		qDebug()<<"mouse press yaptı hemide dede";

//	DiagramItem *item;
//	switch (myMode) {
//	case InsertItem:
//		item = new DiagramItem(myItemType, myItemMenu);
//		item->setBrush(myItemColor);
////		addItem(item);
//		item->setPos(mouseEvent->pos());
//		emit itemInserted(item);
//		break;
//		//! [6] //! [7]
//	case InsertLine:
//		line = new QGraphicsLineItem(QLineF(mouseEvent->pos(),
//											mouseEvent->pos()));
//		line->setPen(QPen(myLineColor, 2));
////		addItem(line);
//		break;
//		//! [7] //! [8]
//	case InsertText:
//		textItem = new DiagramTextItem();
//		textItem->setFont(myFont);
//		textItem->setTextInteractionFlags(Qt::TextEditorInteraction);
//		textItem->setZValue(1000.0);
//		connect(textItem, SIGNAL(lostFocus(DiagramTextItem*)),
//				this, SLOT(editorLostFocus(DiagramTextItem*)));
//		connect(textItem, SIGNAL(selectedChange(QGraphicsItem*)),
//				this, SIGNAL(itemSelected(QGraphicsItem*)));
////		addItem(textItem);
//		textItem->setDefaultTextColor(myTextColor);
//		textItem->setPos(mouseEvent->pos());
//		emit textInserted(textItem);
//		//! [8] //! [9]
//	default:
//		;
//	}
}
//! [9]

//! [10]
void DiagramView::mouseMoveEvent(QMouseEvent *mouseEvent)
{
	if (myMode == InsertLine && line != 0) {
		QLineF newLine(line->line().p1(), mouseEvent->pos());
		line->setLine(newLine);
	} else if (myMode == MoveItem) {
	}
}
//! [10]

//! [11]
void DiagramView::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
//	if (line != 0 && myMode == InsertLine) {
//		QList<QGraphicsItem *> startItems = items(line->line().p1());
//		if (startItems.count() && startItems.first() == line)
//			startItems.removeFirst();
//		QList<QGraphicsItem *> endItems = items(line->line().p2());
//		if (endItems.count() && endItems.first() == line)
//			endItems.removeFirst();

//		removeItem(line);
//		delete line;
//		//! [11] //! [12]

//		if (startItems.count() > 0 && endItems.count() > 0 &&
//				startItems.first()->type() == DiagramItem::Type &&
//				endItems.first()->type() == DiagramItem::Type &&
//				startItems.first() != endItems.first()) {
//			qDebug()<<startItems.first()->type();
//			DiagramItem *startItem = qgraphicsitem_cast<DiagramItem *>(startItems.first());
//			DiagramItem *endItem = qgraphicsitem_cast<DiagramItem *>(endItems.first());
//			Arrow *arrow = new Arrow(startItem, endItem);
//			qDebug("start item name: %s", startItem->nameee.data());
//			qDebug("end item name: %s", endItem->nameee.data());
//			arrow->setColor(myLineColor);
//			startItem->addArrow(arrow);
//			endItem->addArrow(arrow);
//			arrow->setZValue(-1000.0);
//			addItem(arrow);
//			arrow->updatePosition();
//		}
//	}
//	//! [12] //! [13]
//	line = 0;
//	QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void DiagramView::dropEvent(QDropEvent *event)
{
	qDebug("yo  you!");
}

void DiagramView::dragEnterEvent(QDragEnterEvent *event)
{
	qDebug("dam boi you got this ");
	event->acceptProposedAction();
}
//! [13]

//! [14]
bool DiagramView::isItemChange(int type)
{
//	foreach (QGraphicsItem *item, selectedItems()) {
//		if (item->type() == type)
//			return true;
//	}
//	return false;
}
//! [14]
