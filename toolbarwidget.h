#ifndef TOOLBARWIDGET_H
#define TOOLBARWIDGET_H

#include <QWidget>
#include <QToolButton>

class ToolBarWidget : public QToolButton
{
	Q_OBJECT
public:
	explicit ToolBarWidget(QWidget *parent = nullptr);
	void setName(QString s) {widgetName = s;}

protected:
	void mousePressEvent(QMouseEvent *event) override;
	QString widgetName;

};

#endif // TOOLBARWIDGET_H
