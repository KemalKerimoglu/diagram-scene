QT += widgets
requires(qtConfig(fontcombobox))

HEADERS	    =   mainwindow.h \
		arrow_copy.h \
		connectiondatabase.h \
		diagramitem.h \
		arrow.h \
		diagramtextitem.h \
#		diagramview.h \
		toolbarwidget.h
SOURCES	    =   mainwindow.cpp \
		connectiondatabase.cpp \
		diagramitem.cpp \
#		diagramview.cpp \
		main.cpp \
		arrow.cpp \
		diagramtextitem.cpp \
		toolbarwidget.cpp
RESOURCES   =	diagramscene.qrc


# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/diagramscene
INSTALLS += target
