#include "connectiondatabase.h"
#include <QDebug>

ConnectionDatabase *ConnectionDatabase::instance()
{
	static ConnectionDatabase *inst = nullptr;
	if (!inst)
		inst = new ConnectionDatabase;

	return inst;
}

ConnectionDatabase::ConnectionDatabase()
{
}
