#include "toolbarwidget.h"
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>

ToolBarWidget::ToolBarWidget(QWidget *parent) : QToolButton(parent)
{
}

void ToolBarWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {

		QDrag *drag = new QDrag(this);
		QMimeData *mimeData = new QMimeData;

		mimeData->setText(widgetName);
		drag->setMimeData(mimeData);

		qDebug("drag event, widget name: %s", widgetName.toStdString().data());

		Qt::DropAction dropAction = drag->exec();
	}
}


